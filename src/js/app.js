import { RandomGenerator } from './random-generator';
import '../css/main.scss';

let obj = {
    name: 'Max',
    age: 27,
    greet: function() {
        console.log('Hello there!');
    }
};

let {name, greet: hello} = obj;

hello();
